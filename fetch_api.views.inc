<?php

/**
 * @file
 * Provide Views integration to any orders created with this module.
 */
 
function fetch_api_views_data() {
  /* Table Data */
  $data['fetch_api_orders']['table']['group'] = t('Fetch API');
  
  // Make available as a base table
  $data['fetch_api_orders']['table']['base'] = array(
    'field' => 'first_name',
    'title' => t('Fetch API Orders'),
    'help' => t('Access records for each Fetchpp.com API order made from Drupal'),
  );
  
  // Table fields
  $data['fetch_api_orders']['first_name'] = array(
    'title' => t('First name'),
    'help' => t('First name of the person who placed the fetch order'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
      ),
    'argument' => array('handler' => 'views_handler_argument_string'),
    'filter' => array('handler' => 'views_handler_filter_string'),
    'sort' => array('handler' => 'views_hander_sort_string'),
  );
  $data['fetch_api_orders']['last_name'] = array(
    'title' => t('Last name'),
    'help' => t('Last name of the person who placed the fetch order'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
      ),
    'argument' => array('handler' => 'views_handler_argument_string'),
    'filter' => array('handler' => 'views_handler_filter_string'),
    'sort' => array('handler' => 'views_hander_sort_string'),
  );
  $data['fetch_api_orders']['mail'] = array(
    'title' => t('Email'),
    'help' => t('Email address of the person who placed the fetch order'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
      ),
    'argument' => array('handler' => 'views_handler_argument_string'),
    'filter' => array('handler' => 'views_handler_filter_string'),
    'sort' => array('handler' => 'views_hander_sort_string'),
  );
  $data['fetch_api_orders']['total_items'] = array(
    'title' => t('Total items'),
    'help' => t('Total number of items in the order'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
      ),
    'argument' => array('handler' => 'views_handler_argument_string'),
    'filter' => array('handler' => 'views_handler_filter_string'),
    'sort' => array('handler' => 'views_hander_sort_string'),
  );
  $data['fetch_api_orders']['order_items'] = array(
    'title' => t('Order Items'),
    'help' => t('List of all SKUs contained in the fetch order'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
      ),
    'argument' => array('handler' => 'views_handler_argument_string'),
    'filter' => array('handler' => 'views_handler_filter_string'),
    'sort' => array('handler' => 'views_hander_sort_string'),
  );
  $data['fetch_api_orders']['timestamp'] = array(
    'title' => t('Order date'),
    'help' => t('Timestamp of the fetch order'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
      ),
    'argument' => array('handler' => 'views_handler_argument_date'),
    'filter' => array('handler' => 'views_handler_filter_date'),
    'sort' => array('handler' => 'views_handler_sort_date'),
  );
  
  return $data;
}