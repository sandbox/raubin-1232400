<?php

/**
 * @file Provides fetch api settings form
 */

/**
 * Settings form
 */
function fetch_api_settings_form() {
  $form['fetch_api_handle'] = array(
    '#title' => t('Fetch API Handle'),
    '#type' => 'textfield',
    '#description' => t('Provide your fetch handle. Your <em>handle</em> was defined when you created your Fetch account and is how you access your account from the web. For example, http://demo.fetchapp.com, where "demo" is your handle. <strong>Do not include the full URL.</strong>'),
    '#default_value' => variable_get('fetch_api_handle', NULL),
  );
  $form['fetch_api_key'] = array(
    '#title' => t('Fetch API Key'),
    '#type' => 'textfield',
    '#description' => t('Provide your Fetchapp API key.'),
    '#default_value' => variable_get('fetch_api_key', NULL),
  );
  $form['fetch_api_token'] = array(
    '#title' => t('Fetch API Token'),
    '#type' => 'password',
    '#description' => t('Provide your Fetchapp API token. <span style="color:red;">This is hidden from view after saving</span>.'),
    '#default_value' => variable_get('fetch_api_token', NULL),
  );
  $form['fetch_api_order_prefix'] = array(
    '#title' => t('Order number prefix'),
    '#type' => 'textfield',
    '#description' => t('Provide an order prefix for API orders. An order number must be provided with each order request. To avoid conflicts with orders created through other methods, it is recommended to use a prefix for API orders. If an order number already exists in Fetch, the order request will fail. If you have already exceeded 99,000 orders in fetch, you should make this number larger. Default is 9999 (+ the auto incrementing id in the database tacked on the end, not added.)'),
    '#default_value' => variable_get('fetch_api_order_prefix', '9999'),
  );
  
  return system_settings_form($form);
}